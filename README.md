# datld-210105-MPS-Assignment-git

This repository is a assignment submit for senior unity developer testing program.

## Platforms Supported

- Windows 10 x64
- Windows ARM64
- Android ARMv7a
- Android ARMv8a

## Development

**Any contributions to this repository need to be done using Unity 2019.4 LTS**

This unity version was chosen as it is Long Term Supported and provides compatibility with all targeted platfroms.

There is no reason why this asset could not be used by earlier versions of Unity, but those versions might not easily support all of our targeted platforms.

To create a development environment, create a new 3D project in Unity and follow the `Setup the repository inside your Unity project` instructions

## Installation

### From Git

#### Install Git LFS
This project uses Git LFS for data (.csv, .json state files) and binaries (libraries in `Plugins`), so ensure that [Git LFS is installed](https://git-lfs.github.com/) and that you've run `git lfs install`.

### Setup the repository inside your Unity project
To be able to access the Pulse Unity Asset, this whole repository needs to live somewhere under the `Assets` folder of your Unity project. For example:
```
YourProject/
  Assembly-CSharp-Editor.csproj
  Assembly-CSharp.csproj
  Assets/
    PulsePhysiologyEngine/ <-- make this folder here
    ...
  Library/
  YourProject.sln
  obj/
  ProjectSettings/
  Temp/
  ...
```

Therefore:
```
mkdir YourProject/Assets/PulsePhysiologyEngine
cd YourProject/Assets/PulsePhysiologyEngine
git clone https://gitlab.kitware.com/physiology/unity .
```

If `YourProject` is version-controlled with Git, you can add our asset as a submodule instead:
```bash
mkdir YourProject/Assets/PulsePhysiologyEngine
cd YourProject/Assets/PulsePhysiologyEngine
git submodule add https://gitlab.kitware.com/physiology/unity
```

## How to play

###FOR WINDOW
A,S,D,W to Move
Left Mouse to Shoot
Esc to open setting Window

###FOR ANDROID
Use Ingame TouchPad to Control

## Limitations
This project may have some bugs since the limitation of time to finish.