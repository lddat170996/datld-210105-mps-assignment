﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MobileInputButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Action<PointerEventData> _OnPointerUp;
    public Action<PointerEventData> _OnPointerDown;

    public void OnPointerDown(PointerEventData eventData)
    {
        _OnPointerDown?.Invoke(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _OnPointerUp?.Invoke(eventData);
    }
}
