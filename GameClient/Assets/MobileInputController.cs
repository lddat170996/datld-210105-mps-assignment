﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileInputController : BaseHUD
{
    public static MobileInputController instance;

    private void Awake()
    {
        instance = this;
    }

    public FixedJoystick _fixedJoyStick;
    public MobileInputButton _btnJump;
    public MobileInputButton _btnShoot;
    public MobileInputButton _btnPurchase;
    public MobileInputButton _btnReload;
    public TouchCameraInput _touchCamera;

}
