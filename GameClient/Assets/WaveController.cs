﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using System;
//using MEC;
//using Ez.Pooly;

//[Serializable]
//public class LevelSpawnData
//{
//    public int Level;
//    public List<WaveDispenser> WaveDispensers;
//    public float DelayEachWave;
//}

//[Serializable]
//public class WaveDispenser
//{
//    public int TotalZombie;
//    public List<ZombieRatio> ZombieRatios;
//}

//[Serializable]
//public class ZombieRatio
//{
//    public ZOMBIE_TYPE Type;
//    public float percentNum;
//}

//public class WaveController : MonoBehaviour
//{
//    private int CurLevel;
//    private int CurWave;

//    private LevelSpawnData _curLevelData;
//    private ResourcesManager _resourceManager;
//    private List<Zombie> _levelZombies;

//    public void StartLevel(int level)
//    {
//        _curLevelData = ResourcesManager.instance._listLevelData.GetLevelData(level);
//    }

//    public IEnumerator<float> SpawnWave(WaveDispenser wave)
//    {
//        Dictionary<ZOMBIE_TYPE, float> _dictRandZombie = new Dictionary<ZOMBIE_TYPE, float>();
//        for (int i = 0; i < wave.TotalZombie; i++)
//        {
//            var randZom = wave.ZombieRatios.RandomElementByWeight(x => x.percentNum);
//            SpawnZombie(randZom.Type);
//            yield return Timing.WaitForSeconds(1.0f);
//        }
//    }

//    public void SpawnZombie(ZOMBIE_TYPE type)
//    {
//        var trf = Pooly.Spawn(type.ToString().Trim(), Vector3.zero, Quaternion.identity);
//        var zombie = trf.GetComponent<Zombie>();
//    }

//}
