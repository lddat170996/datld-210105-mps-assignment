﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDGameSetting : BaseHUD
{
    public override void PreInit(EnumHUD type, IParentHud _parent, params object[] args)
    {
        base.PreInit(type, _parent, args);
      
    }

    public override void Show(Action<bool> showComplete = null, bool addStack = true)
    {
        base.Show(showComplete, addStack);
        GameManager.instance.SetDisableLockCursor(true);
        GameManager.instance.SetPauseGame(true);

    }

    public override void Hide(Action<bool> hideComplete = null)
    {
        base.Hide(hideComplete);
        GameManager.instance.SetPauseGame(false);
    }

    public void OnButtonHome()
    {
        GameMaster.instance.BackToHome();
        Hide();
    }
}
