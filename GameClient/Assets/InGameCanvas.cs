﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameCanvas : BaseParentHUD
{
    public static InGameCanvas instance;

    private void Awake()
    {
        instance = this;
    }

    public RectTransform _deadScene;
    public Text _txtPlayerHP;


    #region Button

    public void OnButtonReplay()
    {
        _deadScene.gameObject.SetActiveIfNot(false);
    }

    public void OnButtonSetting()
    {
        Debug.LogError("show setting");
        ShowHUD(EnumHUD.HUD_GAME_SETTING);
    }

    #endregion
}
