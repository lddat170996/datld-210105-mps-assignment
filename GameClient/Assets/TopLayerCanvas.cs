﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopLayerCanvas : BaseParentHUD
{
    public static TopLayerCanvas instance;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
}
