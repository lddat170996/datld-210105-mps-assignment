﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Chasing : MonoBehaviour
{
    GameManager networkManager;
    Animator animator;
    HealthManager healthManager;
    NavMeshAgent agent;
    AudioSource audioSource;
    public GameObject target;
    public float damage = 15.0f;
    public bool isAttacking = false;
    public bool shouldChase = true;
    public bool isInLateUpdate = false;
    public bool shouldUpdate = true;

    public AudioClip attackSound;
    public AudioClip deathSound;

    private void Awake()
    {
        shouldUpdate = false;
    }

    // Use this for initialization
    public void Initialize(Animator _animator)
    {
        animator = _animator;
        healthManager = GetComponent<HealthManager>();
        agent = GetComponent<NavMeshAgent>();
        audioSource = GetComponent<AudioSource>();

        networkManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        SetAnimAttack(false);
        shouldUpdate = true;
    }

    IEnumerator distUpdateCo = null;

    // Update is called once per frame
    void Update()
    {
        
        if (!shouldUpdate || GameManager.instance.PauseGame) return;

        if (!healthManager.IsDead)
        {
            if (!isAttacking)
            {
                // NetworkPlayer targetNetworkPlayer = target.GetComponent<NetworkPlayer>();

                // if(targetNetworkPlayer.IsLocalPlayer) {
                float distance = GetActualDistanceFromTarget();

                // Reduce calculation of path finding
                if (distance <= 20f)
                {
                    if (distUpdateCo != null)
                    {
                        StopCoroutine(distUpdateCo);
                    }

                    isInLateUpdate = false;
                    agent.destination = target.transform.position;
                }
                else if (!isInLateUpdate)
                {
                    if (distance <= 40f)
                    {
                        distUpdateCo = LateDistanceUpdate(2f);
                        StartCoroutine(distUpdateCo);
                    }
                    else if (distance <= 60)
                    {
                        distUpdateCo = LateDistanceUpdate(3f);
                        StartCoroutine(distUpdateCo);
                    }
                    else if (distance <= 80)
                    {
                        distUpdateCo = LateDistanceUpdate(4f);
                        StartCoroutine(distUpdateCo);
                    }
                    else
                    {
                        distUpdateCo = LateDistanceUpdate(5f);
                        StartCoroutine(distUpdateCo);
                    }
                }
                // }
            }

            animator.SetFloat("SpeedMultiplier", agent.speed);

            if (agent.pathPending) return;

            CheckAttack();
        }
        else
        {
            shouldUpdate = false;
            shouldChase = false;

            audioSource.PlayOneShot(deathSound);

            Destroy(agent);

            StartCoroutine(RemoveGameObject());
            return;
        }
    }

    IEnumerator LateDistanceUpdate(float duration)
    {
        isInLateUpdate = true;
        agent.destination = target.transform.position;
        yield return new WaitForSeconds(duration);

        isInLateUpdate = false;
        distUpdateCo = null;
        yield break;
    }

    float GetActualDistanceFromTarget()
    {
        return GetDistanceFrom(target.transform.position, this.transform.position);
    }

    float GetDistanceFrom(Vector3 src, Vector3 dist)
    {
        return Vector3.Distance(src, dist);
    }

    float origSpeed;

    void CheckAttack()
    {
        // Calculate actual distance from target
        float distanceFromTarget = GetActualDistanceFromTarget();

        // Calculate direction is toward player
        Vector3 direction = target.transform.position - this.transform.position;
        float angle = Vector3.Angle(direction, this.transform.forward);

        if (!isAttacking && distanceFromTarget <= 2.0f && angle <= 60f)
        {
            isAttacking = true;
            shouldChase = false;

            origSpeed = agent.speed;
            agent.speed = 0;

            audioSource.PlayOneShot(attackSound);
            SetAnimAttack(true);

            HealthManager targetHealthManager = target.GetComponent<HealthManager>();

            if (targetHealthManager)
            {
                targetHealthManager.ApplyDamage(damage);
            }

            StartCoroutine(ResetAttacking());
        }
    }

    private void SetAnimAttack(bool _attack)
    {
        //animator.SetTrigger("Attack");
        animator?.SetBool("b_attack", _attack);
    }

    IEnumerator ResetAttacking()
    {
        yield return new WaitForSeconds(1.4f);

        isAttacking = false;
        shouldChase = true;
        SetAnimAttack(false);

        if (!healthManager.IsDead)
        {
            agent.speed = origSpeed;
        }


        yield break;
    }

    IEnumerator RemoveGameObject()
    {
        yield return new WaitForSeconds(5f);
        // PhotonNetwork.Destroy(gameObject);
        Destroy(gameObject);
    }
}
