﻿using Ez.Pooly;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeEnemy
{
    BIO,
    MECH
}

public class EnemyType : MonoBehaviour
{
    public TypeEnemy type;

    public Transform _visualTransform;
    public HealthManager health;
    public Chasing chasing;

    private Animator _zomAnim;

    private void Start()
    {
        var zomPref = PrefabManager.instance.GetRandZombieVariant();
        for (int i = _visualTransform.childCount - 1; i >= 0; i--)
        {
            Pooly.Despawn(_visualTransform.GetChild(i).transform);
        }

        var trf = Pooly.Spawn(zomPref.transform, transform.position, Quaternion.identity, _visualTransform);
        trf.transform.localPosition = Vector3.zero;
        trf.transform.localRotation = Quaternion.identity;
        trf.transform.localScale = Vector3.one;
        _zomAnim = trf.GetComponent<Animator>();

        health.Initialize(_zomAnim);
        chasing.Initialize(_zomAnim);

    }
}