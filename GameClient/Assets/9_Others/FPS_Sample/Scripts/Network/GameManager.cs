﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private void Awake()
    {
        instance = this;
    }

    [SerializeField] string version = "v0.0.1";
    [SerializeField] string roomName = "zombie-fps-test";
    [SerializeField] string playerName = "Player";
    [SerializeField] List<GameObject> players = new List<GameObject>();
    public GameObject player;
    public Transform spawnPoint;
    public GameObject enemySpawner;
    public GameObject lobbyCam;
    public GameObject lobbyUI;
    public GameObject inGameUI;
    public Text statusText;

    private bool IsShowGameSetting = false;

    public bool PauseGame { get; private set; }

    public void SetPauseGame(bool _value)
    {
        PauseGame = _value;
    }

    public bool DisableLockCursor { get; private set; }
    public void SetDisableLockCursor(bool _value)
    {
        DisableLockCursor = _value;

        if (DisableLockCursor)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public List<GameObject> Players {
        get {
            return players;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!IsShowGameSetting)
            {
                InGameCanvas.instance.OnButtonSetting();
                IsShowGameSetting = true;
            }
            else
            {
                InGameCanvas.instance.HideHUD(EnumHUD.HUD_GAME_SETTING);
                IsShowGameSetting = false;
            }

        }
    }

    void Start()
    {
        lobbyCam.SetActive(false);
        lobbyUI.SetActive(false);
        SetDisableLockCursor(false);

        GameObject playerObj = Instantiate(player, spawnPoint.position, spawnPoint.rotation);

        inGameUI.SetActive(true);
        enemySpawner.SetActive(true);
        enemySpawner.GetComponent<EnemySpawner>().target = playerObj;

        SetPauseGame(false);
    }

    public void ReplayGame()
    {
        enemySpawner.GetComponent<EnemySpawner>().ReplayLevel();
        player.GetComponent<Player>().RestartLevel();
        SetDisableLockCursor(false);

    }

}
