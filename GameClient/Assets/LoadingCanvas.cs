﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LoadingCanvas : BaseParentHUD
{
    public static LoadingCanvas instance;

    public Slider _loadingBar;
    
    private void Awake()
    {
        instance = this;
    }

    public override void Initialize()
    {
        base.Initialize();
        _loadingBar.maxValue = 100;
        _loadingBar.value = 0;
    }

    public void SetLoadingValue(float percent)
    {
        this._loadingBar.value = percent;
    }
}
