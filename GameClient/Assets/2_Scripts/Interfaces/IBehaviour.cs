﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBehaviour
{
    bool IsApplied { get; set; }

    void ApplyBehaviour();
    void Initialize(params object[] _params);
    void UpdateBehaviour(float deltaTime);
    void StopBehaviour();
}
