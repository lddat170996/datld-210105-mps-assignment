﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISystem
{
    bool DontDestroyOnLoad { get; }
    void Initialize(params object[] _params);
    void UpdateSystem(float _deltaTime);
    void CleanUp();
}
