﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface IParentHud
{
    void Initialize();

    BaseHUD ShowHUD(EnumHUD _type, Action<bool> ShowComplete = null, params object[] args);
    BaseHUD HideHUD(EnumHUD _type, Action<bool> HideComplete = null);

    Stack<EnumHUD> MenuStacks { get; }
    void EnqueueStacks(EnumHUD hud);
    void DequeueStacks(bool refresh);
}