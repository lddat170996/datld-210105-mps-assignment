﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConstant
{
    public static string SCENE_LOADING = "LoadingScene";
    public static string SCENE_MAIN_MENU = "MainMenu";
    public static string SCENE_GAME_PLAY = "GamePlayScene";

#if UNITY_EDITOR
    public static bool IsMobileInput = false;
#else
    public static bool IsMobileInput = true;
#endif

}


public static class TagConstant
{
    public static string TAG_ZOMBIE = "zombie";
    public static string TAG_ATTACKABLE_TRIGGEER = "attackableTrigger";
    public static string TAG_GROUND = "ground";
    public static string TAG_CLICKABLE = "clickable";
}


public static class POOLY_PREF
{
    public static string GetHUDPrefabByType(EnumHUD hudType)
    {
        return hudType.ToString().Trim();
    }
}

public static class EVENT_NAME
{

}
