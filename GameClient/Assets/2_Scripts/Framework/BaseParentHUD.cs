﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ez.Pooly;
using System;
using DG.Tweening;
using Ez.Pooly;
using UnityEngine.UI;
using MEC;

public class BaseParentHUD : MonoBehaviour, IParentHud
{
    public Canvas canvas { get; private set; }

    protected Dictionary<EnumHUD, BaseHUD> _dictHUD;
    private BaseHUD currentHUD;
    public BaseHUD CurrentHUD => currentHUD;
    protected Action<EnumHUD> onShowHUD;
    protected Action<EnumHUD> onResetLayer;

    private void Start()
    {
        Initialize();
    }

    public virtual void Initialize()
    {
        canvas = GetComponent<Canvas>();
        _dictHUD = new Dictionary<EnumHUD, BaseHUD>();
    }

    public virtual BaseHUD ShowHUD(EnumHUD _type, Action<bool> ShowComplete = null, params object[] args)
    {
        BaseHUD _hud = null;
        _dictHUD.TryGetValue(_type, out _hud);
        if (_hud == null)
        {
            var trf = Pooly.Spawn(_type.ToString().Trim(), Vector3.zero, Quaternion.identity);
            _hud = trf.GetComponent<BaseHUD>();
            _hud.transform.SetParent(transform);
            _hud.transform.localPosition = Vector3.zero;
            _hud.transform.localScale = Vector3.one;
            _dictHUD.Add(_type, _hud);
        }

        _hud.transform.SetAsLastSibling();
        _hud.PreInit(_type, this, args);
        _hud.Show(ShowComplete);
        currentHUD = _hud;
        onShowHUD?.Invoke(currentHUD._hudType);

        return _hud;
    }

    public virtual BaseHUD HideHUD(EnumHUD _type, Action<bool> HideComplete = null)
    {
        BaseHUD _hud = null;
        _dictHUD.TryGetValue(_type, out _hud);
        if (_hud)
        {
            _hud.Hide(HideComplete);
        }

        return _hud;
    }

    public virtual BaseHUD GetHUD(EnumHUD _type)
    {
        BaseHUD result = null;
        _dictHUD.TryGetValue(_type, out result);

        return result;
    }

    #region FLoating Text

    public virtual void ShowFloatingText(Vector3 screenPos, string content, int size, float moveHeight, Color color,
        float duration = 0.3f)
    {

    }


    #endregion

    #region MenuStacks

    public Stack<EnumHUD> stackMenu { get; private set; }

    Stack<EnumHUD> IParentHud.MenuStacks => menuStacks;

    private Stack<EnumHUD> menuStacks = new Stack<EnumHUD>();

    public void EnqueueStacks(EnumHUD hud)
    {
        menuStacks.Push(hud);
    }

    public void DequeueStacks(bool isRefresh)
    {
        if (menuStacks.Count <= 0)
            return;
        menuStacks.Pop();
        //get top
        var arr = menuStacks.ToArray();
        if (arr != null && arr.Length >= 1)
        {
            var topOne = arr[0];
            if (topOne != EnumHUD.NONE)
            {
                var topHUD = GetHUD(topOne);
                currentHUD = topHUD;
                if (isRefresh)
                    topHUD?.ResetLayers();
            }
        }
    }

    public void OnResetLayer(EnumHUD type)
    {
        onResetLayer?.Invoke(type);
    }

    #endregion
}