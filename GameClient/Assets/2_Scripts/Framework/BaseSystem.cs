﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseSystem : MonoBehaviour, ISystem
{
    [SerializeField]
    private bool _dontDestroyOnLoad = true;
    bool ISystem.DontDestroyOnLoad { get => _dontDestroyOnLoad; }

    private void Awake()
    {
        if (_dontDestroyOnLoad)
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public abstract void Initialize(params object[] _params);
    public abstract void UpdateSystem(float _deltaTime);
    public abstract void CleanUp();


}
