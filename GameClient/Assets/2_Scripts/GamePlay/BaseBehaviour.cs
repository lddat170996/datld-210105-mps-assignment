﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBehaviour : MonoBehaviour, IBehaviour
{
    private bool isApplied = false;
    public bool IsApplied { get => isApplied; set => isApplied = value; }

    public virtual void ApplyBehaviour()
    {
        this.IsApplied = true;
    }

    public virtual void Initialize(params object[] _params)
    {

    }

    public virtual void StopBehaviour()
    {
        IsApplied = false;
    }

    public virtual void UpdateBehaviour(float deltaTime)
    {
        if (!IsApplied)
            return;
    }
}
