﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using System;
//using MEC;
//using Ez.Pooly;

//public enum ZOMBIE_TYPE
//{
//    NONE,
//    ZOMBIE_NORMAL,
//    ZOMBIE_MIDDLE,
//    ZOMBIE_HARD,
//    ZOMBIE_BOSS
//}


//[SerializeField]
//public class ZombieData
//{
//    public float WalkingSpeed;
//    public float Dmg;
//    public float FireRate;
//    public float MaxHP;
//    public float ChasingRadius;

//}

//public class Zombie : MonoBehaviour
//{
//    public ZOMBIE_TYPE Type = ZOMBIE_TYPE.NONE;
//    public ZombieData Data { get; private set; }
//    public Animator _animator;

//    //[Space(10)]
//    //[Header("Behaviours")]
//    //[SerializeField]
//    // private EnemyFollow _enemyFollow;

//    //[SerializeField]
//    //private Health _zomHealth;

//    private void Start()
//    {
//        Initialize(new ZombieData()
//        {
//            WalkingSpeed = 2f,
//            Dmg = 1.0f,
//            FireRate = 1.0f,
//            MaxHP = 10,
//            ChasingRadius = 10f
//        });
//    }

//    public void Initialize(ZombieData _data)
//    {
//        Data = _data;

//        //HP
//        _zomHealth.Initialize();
//        _zomHealth.ResetMaxHP(_data.MaxHP);
//        _zomHealth.OnDie += OnZombieDie;
//        _zomHealth.OnHpChanged += OnZomHPChanged;
//        _zomHealth.ApplyBehaviour();

//        //chasing
//        //_enemyFollow.Initialize(this);
//        //_enemyFollow.ApplyBehaviour();
//    }

//    private void OnZomHPChanged(float curHp)
//    {

//    }

//    private void OnZombieDie()
//    {
//        this._animator.SetBool("b_death", true);

//        DestroyZombie();
//    }

//    private void DestroyZombie()
//    {
//        _zomHealth.OnDie -= OnZombieDie;
//        _zomHealth.OnHpChanged -= OnZomHPChanged;
//        _zomHealth.StopBehaviour();

//        // _enemyFollow.StopBehaviour();

//        //GameObject.Destroy(this.gameObject, 1.0f);
//        Pooly.Despawn(this.transform);
//        //Timing.CallDelayed(0.4f, () =>
//        //{
//        //    Pooly.Despawn(this.transform);
//        //});
//    }

//    private void Update()
//    {
//        float _deltaTime = Time.deltaTime;

//        if (_zomHealth != null)
//            _zomHealth.UpdateBehaviour(_deltaTime);

//        //if (_enemyFollow != null)
//        //    _enemyFollow.UpdateBehaviour(_deltaTime);
//    }


//}
