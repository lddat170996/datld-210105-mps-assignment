﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[Serializable]
public class HUDVariant
{
    public EnumHUD _type;
    public BaseHUD _hud;
}


[CreateAssetMenu(fileName = "HUDBankSO", menuName = "ScriptableObjects/HUDBankSO", order = 1)]
public class HUDBankSO : ScriptableObject
{
    [SerializeField]
    private List<HUDVariant> _listHUD;

    public BaseHUD getHUDPrefab(EnumHUD type)
    {
        var hud = _listHUD.FirstOrDefault(x => x._type == type);
        if (hud == null)
        {
            Debug.LogError($"Cant getHUDPrefab {type}");
            return null;
        }

        return hud._hud;
    }

}
