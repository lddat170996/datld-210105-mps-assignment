﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using MEC;
using UnityEngine.SceneManagement;
using Ez.Pooly;

public class GameMaster : BaseSystem
{
    public static GameMaster instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Initialize();
        DontDestroyOnLoad(this.gameObject);
    }

    #region Load Game
    private float _curLoadingPercent = 0f;
    private Action<float> _onLoadingChanged;

    public void StartLoadGame()
    {
        LoadingCanvas.instance.Initialize();
        Timing.RunCoroutine(StartLoadGameCoroutine(OnLoadingPercentChanged));
    }

    private IEnumerator<float> StartLoadGameCoroutine(Action<float> loadingPercent)
    {
        _onLoadingChanged = loadingPercent;
        SetLoadingPercent(10);

        while (_curLoadingPercent < 90f)
        {
            ++_curLoadingPercent;
            SetLoadingPercent(_curLoadingPercent);
            yield return Timing.WaitForOneFrame;
        }

        bool waitTask = true;
        LoadScene(GameConstant.SCENE_MAIN_MENU, (complete) =>
        {
            waitTask = false;
        });
        while (waitTask)
        {
            yield return Timing.WaitForOneFrame;
        }


        loadingPercent?.Invoke(100);

    }

    private void SetLoadingPercent(float newValue)
    {
        _curLoadingPercent = newValue;
        _onLoadingChanged?.Invoke(_curLoadingPercent);

    }

    private void OnLoadingPercentChanged(float curPercent)
    {
        LoadingCanvas.instance.SetLoadingValue(curPercent);
    }

    private void FinishLoadingProcess()
    {

    }

    #endregion

    #region Scene Utils

    public void LoadScene(string sceneName, Action<bool> callback)
    {
        StartCoroutine(LoadSceneAsync(sceneName, callback));
    }

    IEnumerator LoadSceneAsync(string sceneName, Action<bool> callback)
    {
        yield return null;

        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        Debug.Log("Pro :" + asyncOperation.progress);
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }

        if (asyncOperation.isDone)
            callback?.Invoke(true);
    }

    #endregion

    public void GoToGameScene()
    {
        Timing.RunCoroutine(GoToGameSceneCoroutine());
    }

    IEnumerator<float> GoToGameSceneCoroutine()
    {
        TopLayerCanvas.instance.ShowHUD(EnumHUD.HUD_LOADING);
        yield return Timing.WaitForSeconds(0.4f);
        LoadScene(GameConstant.SCENE_GAME_PLAY, (callback) =>
         {
             TopLayerCanvas.instance.HideHUD(EnumHUD.HUD_LOADING);
         });
    }

    public void BackToHome()
    {
        Timing.RunCoroutine(BackToHomeCoroutine());
    }

    IEnumerator<float> BackToHomeCoroutine()
    {
        TopLayerCanvas.instance.ShowHUD(EnumHUD.HUD_LOADING);
        yield return Timing.WaitForSeconds(0.4f);
        LoadScene(GameConstant.SCENE_MAIN_MENU, (callback) =>
        {
            TopLayerCanvas.instance.HideHUD(EnumHUD.HUD_LOADING);

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        });
    }

    public override void Initialize(params object[] _params)
    {
        _curLoadingPercent = 0f;

        StartLoadGame();
    }

    public override void UpdateSystem(float _deltaTime)
    {

    }

    public override void CleanUp()
    {

    }

}
